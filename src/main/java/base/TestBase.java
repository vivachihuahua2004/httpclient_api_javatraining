package base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


// HTTP Client exercise from API https://reqres.in/
public class TestBase {

    public Properties prop;
    private final String configFilePath = "src/main/java/config/config.properties";
    public final int RESPONSE_CODE_200 = 200;
    public final int RESPONSE_CODE_201 = 201;
    public final int RESPONSE_CODE_400 = 400;
    public final int RESPONSE_CODE_401 = 401;
    public final int RESPONSE_CODE_500 = 500;


    public TestBase()  {

        prop = new Properties();

        try {
            FileInputStream inputStream = new FileInputStream(configFilePath);

            prop.load(inputStream);
        }
        catch (IOException e){
            System.out.println(e.getStackTrace());
        }
    }


}
