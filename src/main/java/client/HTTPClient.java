package client;


import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

// HTTP Client exercise from API https://reqres.in/
public class HTTPClient {

    public CloseableHttpResponse getCall(String url)
            throws IOException {

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet getCall = new HttpGet(url);  //REST get Call
        CloseableHttpResponse response = httpClient.execute(getCall);

        return  response;


//        //Extracting Response
//        int statusCode = response.getStatusLine().getStatusCode();
//
//        System.out.println("Status code is : " + statusCode);
//        String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
//
//        //Getting Response JSON
//        JSONObject responseJSON = new JSONObject(responseString);
//        System.out.println("API Responese JSON: " + responseJSON);
//
//        //Getting Response Headers
//        Header[] responseHeaders = response.getAllHeaders();
//        HashMap<String, String> headers = new HashMap<>();
//
//        for (Header header : responseHeaders){
//            headers.put(header.getName(), header.getValue());
//        }
//
//       System.out.println("All the headers are: " + headers);

    }


}
