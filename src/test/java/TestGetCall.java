import base.TestBase;
import client.HTTPClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.TestUtils;

import java.io.IOException;

public class TestGetCall extends TestBase {

    TestBase testBase;
    HTTPClient driver;
    String APIURI;
    CloseableHttpResponse apiResponse;

    @BeforeTest
    public void setup(){

        testBase = new TestBase();
        String  URL = prop.getProperty("URI");
        String pathParam = prop.getProperty("ServiceURI");
        APIURI = URL + pathParam;


    }

    @Test
    public void testGetCall()
            throws IOException {
        System.out.println("The test will be running a call to: " + APIURI);
        driver = new HTTPClient();
        apiResponse = driver.getCall(APIURI);

        System.out.println("Here you can parse and traverse JSON API response and perform Assertions:");
        System.out.println(apiResponse);

        //E.g
        Assert.assertEquals(apiResponse.getStatusLine().getStatusCode(), RESPONSE_CODE_200);

    }


    @Test
    public void testGetCallResponse()
            throws IOException {

        System.out.println("The test will be running a call to: " + APIURI);
        driver = new HTTPClient();
        apiResponse = driver.getCall(APIURI);

        //Parsing response via JSON Object
        String responseString = EntityUtils.toString(apiResponse.getEntity(), "UTF-8");
        JSONObject responseJSON = new JSONObject(responseString);
        System.out.println("API response JSON: " + responseJSON);

        String jsonStringTotalPerPage= TestUtils.getValueByJPath(responseJSON, "/per_page");
        System.out.println("Total per page in response: " + jsonStringTotalPerPage);

        String jsonStringTotal= TestUtils.getValueByJPath(responseJSON, "/total");
        System.out.println("Total nodes in response: " + jsonStringTotal);

        //then from here you can do your Assertions


    }


}
